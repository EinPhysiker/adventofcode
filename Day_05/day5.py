# -*- coding: utf-8 -*-
"""
Created on Mon Dec  5 06:54:38 2022

@author: VH

input:

[T]     [Q]             [S]
[R]     [M]             [L] [V] [G]
[D] [V] [V]             [Q] [N] [C]
[H] [T] [S] [C]         [V] [D] [Z]
[Q] [J] [D] [M]     [Z] [C] [M] [F]
[N] [B] [H] [N] [B] [W] [N] [J] [M]
[P] [G] [R] [Z] [Z] [C] [Z] [G] [P]
[B] [W] [N] [P] [D] [V] [G] [L] [T]
 1   2   3   4   5   6   7   8   9

move 5 from 4 to 9
move 3 from 5 to 1
move 12 from 9 to 6


note:
    one could use deque (avoiding insert(0,...)) and parse,
    did it intentionally without

"""

# either '9000' for part 1 or '9001' for part 2
crane_type = '9001'

stacks = {}
with open('day5.txt') as f:
    # first block
    for line in f.readlines():

        # we have 2 subsequent blocks and the first block
        # is finished before the first "move" line starts
        # thus, we can have independet if statements in one loop

        # first read in stack configuration from file
        if '[' in line:
            for pos in range(1, len(line), 4):
                stack_number = int((pos+3)/4)
                if stack_number not in stacks:
                    stacks[stack_number] = []
                if line[pos] != ' ':
                    stacks[stack_number].insert(0, (line[pos]))

        # now do the moves
        if line.startswith('move'):
            args = line.split()
            # now numbers are in 1,3,5
            num_crates = int(args[1])
            source_stack = int(args[3])
            dest_stack = int(args[5])

            crane = []
            for n in range(1, num_crates+1):
                # crate by crate
                if crane_type == '9000':
                    crane.append(stacks[source_stack].pop())
                # multiple crates at once
                else:
                    crane.insert(0, stacks[source_stack].pop())
            for crate in crane:
                stacks[dest_stack].append(crate)

# extract top crates:
answer = ''
for stack in range(1, len(stacks)+1):
    answer += stacks[stack].pop()

print(f"Answer string = {answer}")
