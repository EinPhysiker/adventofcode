# -*- coding: utf-8 -*-
"""
Created on Thu Dec 15 17:05:42 2022

@author: VH

Note to myself: when changing from test input to real input keep in mind
to also change the line number. This avoids unnecessary debugging ...

"""

import re
import operator

reg = (r"Sensor at x=(-?\d+), y=(-?\d+): "
       "closest beacon is at x=(-?\d+), y=(-?\d+)")


# sensor class
class sensor:

    def __init__(self, c):
        # sensor position
        self.sx = c[0]
        self.sy = c[1]
        # beacon position
        self.bx = c[2]
        self.by = c[3]
        # distance to beacon
        self.dist = abs(self.sx - self.bx) + abs(self.sy - self.by)

    def __repr__(self):
        return(f"Sesor at x={self.sx}, y={self.sy}:"
               f" closest beacon is at x={self.bx}, y={self.by},"
               f" distance={self.dist}")

    # return the range the sensor covers in line y with
    # at most the distance of the beacon
    def max_range(self, y):
        length_in_y = self.dist - abs(y-self.sy)
        if length_in_y < 0:
            return None, None
        return self.sx - length_in_y, self.sx + length_in_y

    # check if line y has a beacon - needs to be accounted for later
    def has_beacon(self, y):
        if self.by == y:
            return self.bx
        return None


# check the covered areas in one line and count the covered fields
def search_line(sensors, line):
    beacons = set()
    ranges = []

    # check all sensors for this line
    # append the range to ranges and add the beacon position to beacons
    for s in sensors:
        rmin, rmax = s.max_range(line)
        if rmin is None:
            continue
        ranges.append([rmin, rmax])
        b = s.has_beacon(line)
        if b is not None:
            beacons.add(b)

    # just for completeness if the search failed
    if not ranges:
        return [], beacons, 0

    # sort the ranges, primary key = lower edge, secondary key = higher edge
    ranges.sort(key=operator.itemgetter(0, 1))

    # now compress the ranges: merge overlapping ranges
    compressed_ranges = []
    e1 = ranges[0]
    for en in ranges[1:]:
        if en[0] <= e1[1] + 1:
            e1[1] = max(e1[1], en[1])
            continue
        compressed_ranges.append(e1)
        e1 = en
    compressed_ranges.append(e1)

    # calculate the number of fields covered
    # remove the number of beacons in that line
    length = 0
    for r in compressed_ranges:
        length += r[1] - r[0] + 1
    length -= len(beacons)

    return compressed_ranges, beacons, length


# Read in the file line by line
with open('day15.txt') as f:
    # decode sensors
    result = re.findall(reg, f.read())
    # create a list of sensor objects from the input
    sensors = [sensor([int(x) for x in input]) for input in result]

# part 1

# remember to change line number to 10 for test input!
# line = 10
line = 2000000

ranges, beacons, length = search_line(sensors, line)

print(f"Number of excluded positions at y={line}:  {length}")

# part 2
#
# now find the only possible location of the beacon
# maybe there are faster ways, but I do a straight forward search
# ymax = 20
# xmax = 20
ymax = 4000000
xmax = 4000000

beacon_x = -1
beacon_y = -1
frequency = -1

# scan through all lines and find a missing spot
for line in range(ymax+1):
    ranges, beacons, length = search_line(sensors, line)
    xr = [0, xmax]
    for r in ranges:
        # range is outside search window, continue with next range
        if r[1] < xr[0] or r[0] > xr[1]:
            continue
        # range covers search window, no free spot
        if r[0] <= xr[0] and r[1] >= xr[1]:
            xr = [-1, -1]
            break
        # search window partly excluded
        if r[1] < xr[1]:
            xr[0] = r[1] + 1
        if r[0] > xr[0]:
            xr[1] = r[0] - 1
    # if the search window is not fully excluded, we are done
    # as the solution is unique
    if xr[0] >= 0:
        beacon_x = xr[0]
        beacon_y = line
        frequency = beacon_x * 4000000 + beacon_y
        break

print(f"Beacon found at x={beacon_x}, y={beacon_y}")
print(f"Frequency is {frequency}")
