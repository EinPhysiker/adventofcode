# -*- coding: utf-8 -*-
"""
Created on Tue Dec 20 20:11:00 2022

@author: VH
"""


class Qube:

    all = []
    min_pos = None
    max_pos = None
    pos_list = []
    
    def __init__(self, pos):
        self.pos = pos[:]
        self.sides = 6
        if not Qube.all:
            Qube.min_pos = pos[:]
            Qube.max_pos = pos[:]
        else: 
            for i in range(3):
                if pos[i] < Qube.min_pos[i]:
                    Qube.min_pos[i] = pos[i]
                if pos[i] > Qube.max_pos[i]:
                    Qube.max_pos[i] = pos[i]
        Qube.all.append(self)
        Qube.pos_list.append(pos)
        

    def __repr__(self):
        return f"Qube at {self.pos} with {self.sides} sides"
    
    def check_sides(self):
        sides = 6
        for other in Qube.all:
            if other == self:
                continue
            if Qube.are_neighbours(self.pos, other.pos):
                sides -= 1
        self.sides = sides
        
    @staticmethod 
    def are_neighbours(pos1, pos2):
        for c in [[0,1,2], [1,2,0], [2,0,1]]:
            if (pos1[c[0]] == pos2[c[0]] and
                pos1[c[1]] == pos2[c[1]]):
                for d in [-1, 1]:
                    if pos1[c[2]] == pos2[c[2]] + d:
                        return True    
        return False

    @staticmethod 
    def get_neighbours(pos):
        neighbours = []
        for c in [0, 1 ,2]:
            for d in [-1, 1]:
                new = pos[:]
                new[c] = new[c] + d
                if (new[c] < Qube.min_pos[c] or
                    new[c] > Qube.max_pos[c]):
                    continue
                neighbours.append(new)
        return neighbours

    @staticmethod 
    def add_margin():
        for i in range(3):
            Qube.min_pos[i] = Qube.min_pos[i] - 1
            Qube.max_pos[i] = Qube.max_pos[i] + 1

    @staticmethod
    def search_outside():
        visited = [ Qube.min_pos[:] ]
        sides = 0
        
        while True:
            still_more = False
            for x in range(Qube.min_pos[0], Qube.max_pos[0] + 1):
                for y in range(Qube.min_pos[1], Qube.max_pos[1] + 1):
                    for z in range(Qube.min_pos[2], Qube.max_pos[2] + 1):
                        check = [x, y, z]
                        if check in visited or check in Qube.pos_list:
                            continue
                        tmp_sides = 0
                        outside = False
                        for n in Qube.get_neighbours(check):
                            if not outside and n in visited:
                                outside = True
                            elif n in Qube.pos_list:
                                tmp_sides += 1
                        if outside:
                            sides += tmp_sides
                            visited.append(check)
                            still_more = True
            if not still_more:
                break
        return sides
                
        
# Read in the file line by line
with open('day18.txt') as f:
    for line in f.readlines():
        pos = [int(x) for x in line.strip().split(',')]
        qube = Qube(pos)

total_sides = 0
for qube in Qube.all:
    qube.check_sides()
    total_sides += qube.sides

print(f"Part 1: Number of exposed sides: {total_sides}")

Qube.add_margin()
outside_sides = Qube.search_outside()

print(f"Part 2: Number of outside sides: {outside_sides}")
