BEGIN	{ 
			full_overlap = 0 
			part_overlap = 0
		}
		{ 
			split($0,a,",") 
			split(a[1],e1,"-")
			split(a[2],e2,"-")

			if (  (e1[2]>=e2[1] && e1[1]<=e2[2]) || 
			      (e2[2]>=e1[1] && e2[1]<=e1[2]) ) {
				part_overlap++
			}

			if ( (e1[1]<=e2[1] && e1[2]>=e2[2]) || 
			     (e2[1]<=e1[1] && e2[2]>=e1[2]) ) {
				full_overlap++
			}
		}
END		{ 
			print "Full overlaps: " full_overlap 
			print "Part overlaps: " part_overlap 
		}
