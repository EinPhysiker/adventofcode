# -*- coding: utf-8 -*-
"""
Created on Sun Dec  4 10:45:09 2022

@author: VH

Assumes valid input syntax per line
A-B,C-D

"""

import sys

full_overlaps = 0
all_overlaps = 0
with open('day4.txt') as f:
    for line in f.readlines():
        sections = []
        try:
            for assignment in line.strip().split(','):
                start, end = [int(x) for x in assignment.split('-')]
                sections.append(set([*range(start, end+1)]))
        except:
            sys.exit(f'Something wrong with input line: {line}')
        if sections[0].issubset(sections[1]) or sections[1].issubset(sections[0]):
            full_overlaps += 1
        if len(sections[0].intersection(sections[1])) > 0:
            all_overlaps += 1

print(f'Number of full overlaps: {full_overlaps}')
print(f'Number of all overlaps:  {all_overlaps}')
