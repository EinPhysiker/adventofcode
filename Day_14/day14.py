# -*- coding: utf-8 -*-
"""
Created on Wed Dec 14 18:39:33 2022

@author: VH

Sorry, today without comments

"""

import re
import numpy as np
import matplotlib.pyplot as plt

reg = r"(\d+,\d+)"


class geomap():

    def __init__(self, min, max, with_floor=False):
        self.with_floor = with_floor
        if with_floor:
            max = max + np.array([0, 2])
        self.min = min
        self.max = max
        self.size = max - min + np.array([1, 1])
        self.map = []
        for y in range(self.size[1]):
            if with_floor and y == self.size[1]-1:
                row = [1 for x in range(self.size[0])]
            else:
                row = [0 for x in range(self.size[0])]
            self.map.append(row)

    def in_map(self, pos):
        for i in range(2):
            if pos[i] < self.min[i] or pos[i] < self.min[i]:
                return False
        for i in range(2):
            if pos[i] > self.max[i] or pos[i] > self.max[i]:
                return False
        return True

    def set_rock(self, pos):
        if not self.in_map(pos):
            return
        pos = pos - self.min
        self.map[pos[1]][pos[0]] = 1

    def set_sand(self, pos):
        if not self.in_map(pos):
            return
        pos = pos - self.min
        self.map[pos[1]][pos[0]] = 2

    def is_blocked(self, pos):
        if not self.in_map(pos):
            return False
        pos = pos - self.min
        return self.map[pos[1]][pos[0]] > 0

    def set_line(self, pos_from, pos_to):
        if not self.in_map(pos_from):
            return
        if not self.in_map(pos_to):
            return
        pos_from = pos_from
        pos_to = pos_to
        if pos_from[0] == pos_to[0]:
            # same x coordinate
            if pos_from[1] < pos_to[1]:
                for y in range(pos_from[1], pos_to[1] + 1):
                    self.set_rock(np.array([pos_from[0], y]))
            else:
                for y in range(pos_to[1], pos_from[1] + 1):
                    self.set_rock(np.array([pos_from[0], y]))
        else:
            # same y coordinate
            if pos_from[0] < pos_to[0]:
                for x in range(pos_from[0], pos_to[0] + 1):
                    self.set_rock(np.array([x, pos_from[1]]))
            else:
                for x in range(pos_to[0], pos_from[0] + 1):
                    self.set_rock(np.array([x, pos_from[1]]))

    def extend_map(self, dir):

        if (dir[0] > 0):
            self.max = self.max + np.array([1, 0])
            self.size = self.max - self.min + np.array([1, 1])
            for row in self.map[0:-1]:
                row.append(0)
            self.map[-1].append(1)
        else:
            self.min = self.min - np.array([1, 0])
            self.size = self.max - self.min + np.array([1, 1])
            for row in self.map[0:-1]:
                row.insert(0, 0)
            self.map[-1].insert(0, 1)

    def drop_sand(self):
        directions = [
            np.array([0, 1]),
            np.array([-1, 1]),
            np.array((1, 1))
            ]

        pos = np.array([500, 0])
        while True:
            moved = False
            for dir in directions:
                if not self.in_map(pos + dir):
                    if self.with_floor:
                        self.extend_map(dir)
                    else:
                        pos += dir
                        break
                if not self.is_blocked(pos + dir):
                    pos += dir
                    moved = True
                    break
            if not moved:
                break

        if self.in_map(pos) and not self.is_blocked(pos):
            self.set_sand(pos)
            return True

        return False

    def draw(self, sub):
        start = np.array([500, 0]) - self.min

        sub.imshow(mymap.map)
        sub.plot(start[0], start[1],
                 marker='o', markersize=5, color='red')


with open('day14.txt') as f:
    map_min = np.array([500, 0])
    map_max = None
    map_traces = []
    for line in f.readlines():
        result = [np.array([int(x), int(y)]) for x, y in [pos.split(',') for pos in re.findall(reg, line)]]
        if type(map_max) == type(None):
            map_max = np.array([max([x[0] for x in result]),
                                max([x[1] for x in result])])
        else:
            for i in range(2):
                map_min[i] = min([map_min[i], min([x[i] for x in result])])
                map_max[i] = max([map_max[i], max([x[i] for x in result])])
        map_traces.append(result)


fig, ax = plt.subplots(2, figsize=(8, 5))
fig.suptitle("Advent of Code: Day 14")

for part in range(2):
    mymap = geomap(map_min, map_max, with_floor=(part == 1))

    for trace in map_traces:
        if len(trace) > 1:
            start = trace[0]
            for next in trace[1:]:
                mymap.set_line(start, next)
                start = next

    count = 0
    while mymap.drop_sand():
        count += 1
    print(f"Part {part+1}; Number of packets of sand at rest: {count}")

    mymap.draw(ax[part])

plt.tight_layout()
plt.show()
