# -*- coding: utf-8 -*-
"""
Created on Wed Dec  7 19:17:53 2022

@author: VH

example input

$ cd /
$ ls
dir a
14848514 b.txt
8504156 c.dat
dir d
$ cd a
$ ls
dir e
29116 f
2557 g
62596 h.lst
$ cd e
$ ls
584 i
$ cd ..
$ cd ..
$ cd d
$ ls
4060174 j
8033020 d.log
5626152 d.ext
7214296 k

"""

max_size = 70000000
space_needed = 30000000


class node:
    def __init__(self, parent, name, size=0):
        self.parent = parent
        self.name = name
        self.size = size
        self.content = None
        if self.parent:
            self.level = self.parent.level + 1
        else:
            self.level = 0

    def get_parent(self):
        return self.parent

    def get_content(self):
        return self.content

    def get_size(self):
        if self.content:
            self.size = 0
            for entry in self.content:
                self.size += entry.get_size()
        return self.size

    def calc_sum_part1(self):
        sum = 0
        if self.content and self.size <= 100000:
            sum += self.size
        if self.content:
            for entry in self.content:
                sum += entry.calc_sum_part1()
        return sum

    def find_directory_part2(self, space_needed):
        global max_size
        name = ""
        size = max_size
        if self.content and self.size > space_needed:
            name = self.name
            size = self.size
        if self.content:
            for entry in self.content:
                name_subdir, size_subdir = entry.find_directory_part2(space_needed)
                if size_subdir < size:
                    name = name_subdir
                    size = size_subdir
        return name, size

    def add_node(self, node):
        if not self.content:
            self.content = []
        self.content.append(node)

    def find_subdir(self, name):
        if self.content:
            for entry in self.content:
                if entry.name == name:
                    return entry
        return None

    def __repr__(self):
        out = ""
        for i in range(0, self.level):
            out += "- "
        out += f"{self.name}: {self.size}"
        if self.content:
            out += " (dir)"
        else:
            out += " (file)"
        return out

    def printtree(self):
        print(self)
        if self.content:
            for entry in self.content:
                entry.printtree()


root = node(parent=None, name="/")
directory = None

with open('day7.txt') as f:
    # strip lines of any characters like crlf etc.
    lines = [line.strip() for line in f.readlines()]
    for line in lines:
        # line can start with '$ cd', '$ ls', 'dir' or number
        # '$ ls' we can skip, 'dir' or number always belongs to last cd
        if line.startswith('$ cd'):
            dirname = line.split()[-1]
            if dirname == '/':
                directory = root
            elif dirname == '..':
                directory = directory.get_parent()
            else:
                subdirectory = directory.find_subdir(dirname)
                if not subdirectory:
                    raise RuntimeError(f"Subdirectory {dirname} does not exist in {directory.name}")
                directory = subdirectory
        elif line.startswith('$ ls'):
            pass
        elif line.startswith('dir'):
            dirname = line.split()[-1]
            subdirectory = node(parent=directory, name=dirname)
            directory.add_node(subdirectory)
        else:
            entry = line.split()
            filename = entry[1]
            filesize = int(entry[0])
            filenode = node(parent=directory, name=filename, size=filesize)
            directory.add_node(filenode)

totalsize = root.get_size()
sum_part1 = root.calc_sum_part1()

print(f"Total filesystem size: {totalsize}")
print(f"Sum of directories <= 100000: {sum_part1}")

free_space = max_size - totalsize
to_be_freed = space_needed - free_space
print(f"To be freed: {to_be_freed}")

# root.printtree()

name, size = root.find_directory_part2(to_be_freed)
print(f"Directory to be deleted: name = {name}, size = {size}")
