# -*- coding: utf-8 -*-
"""
Created on Mon Dec 12 16:52:37 2022

@author: VH

Tree branches are searched simultanously and not completing one path first.
This way visited elements can be excluded as visiting them twice would
lead to a longer path. That means each position is added at most once to a 
path!

For Part 2 the search direction is simply reverted and the end condition
changed to 'a'.

Note: Path solutions are not unique! Only the length is.

"""

import numpy as np
import matplotlib.pyplot as plt

# definitions of directions for easier movement
directions = [
    np.array([1, 0]),
    np.array([0, -1]),
    np.array([-1, 0]),
    np.array([0, 1])
    ]


# check whether point is outside the map
def is_out(pos):
    global map

    if pos[1] < 0 or pos[1] >= len(map):
        return True
    if pos[0] < 0 or pos[0] >= len(map[pos[1]]):
        return True
    return False


map = []
start = []
end = []
# lets define the height we can climb, who knows ...
climb_height = 1

# Read in the file line by line in the map
# store coordinates of start and end and replace S/E by a/z
with open('day12.txt') as f:
    for line in f.readlines():
        if 'S' in line:
            start = [line.index('S'), len(map)]
            line = line.replace('S', 'a')
        if 'E' in line:
            end = [line.index('E'), len(map)]
            line = line.replace('E', 'z')
        map.append([ord(x) - ord('a') for x in line.strip()])

fig, ax = plt.subplots(2, figsize=(8, 5))
fig.suptitle("Advent of Code: Day 12")

# loop over both parts as the changes are minor
for part in range(2):
    # set starting position
    if part == 0:
        pathes = [[start]]
    else:
        pathes = [[end]]
    optimal_path = None

    # create a map for visited position
    visited = []
    # reset visited
    for line in map:
        visited.append([0 for i in range(len(line))])

    # The first solution found is by design the shortest
    # Thus, only continue until a solution is found
    while not optimal_path:
        # new pathes to be further investigated
        new_pathes = []
        # loop over all pathes
        for path in pathes:
            # last position in path
            last = np.array(path[-1])
            # stop condition: reach end or 'a'
            if ((part == 0 and path[-1] == end)
               or (part == 1 and map[last[1]][last[0]] == 0)):
                optimal_path = path
                break
            height_last = map[last[1]][last[0]]

            # from last position go in all 4 directions
            for dir in directions:
                next = last + dir
                # outside of map?
                if is_out(next):
                    continue
                # already visited?
                if visited[next[1]][next[0]]:
                    continue
                height_next = map[next[1]][next[0]]
                # is height difference ok?
                # check has to be reversed for part 2
                if ((part == 0 and height_next - height_last > climb_height)
                   or (part == 1 and height_last - height_next > climb_height)):
                    continue
                # copy path to new path and append next position
                # add to new pathes
                new_path = path[:]
                new_path.append(next.tolist())
                new_pathes.append(new_path)
                # mark position as visited
                visited[next[1]][next[0]] = 1

        # if no new pathes stop searching
        if len(new_pathes) == 0:
            break

        # repeat procedure
        pathes = new_pathes

    if not optimal_path:
        print(f"Part {part+1}: no path found!")
    else:
        # number of steps is number of coordinates - 1
        print(f"Part {part+1}: minimum number of steps: {len(optimal_path)-1}")

    # replace map by visited to show the checked positions on the map
    ax[part].imshow(map, cmap='copper_r', interpolation='none')
    ax[part].plot([pos[0] for pos in optimal_path],
                  [pos[1] for pos in optimal_path],
                  color='blue')

plt.tight_layout()
plt.show()
