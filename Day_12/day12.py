# -*- coding: utf-8 -*-
"""
Created on Mon Dec 12 16:52:37 2022

@author: VH

This is an optimized version without saving the actual path (and, thus, 
no drawing). This saves having a list of all current complete pathes copied
by value in every step.

Tree branches are searched simultanously and not completing one path first.
This way visited elements can be excluded as visiting them twice would
lead to a longer path. That means each position is added at most once to a 
path!

For Part 2 the search direction is simply reverted and the end condition
changed to 'a'.

Note: Path solutions are not unique! Only the length is.

"""

import numpy as np

# definitions of directions for easier movement
directions = [
    np.array([1, 0]),
    np.array([0, -1]),
    np.array([-1, 0]),
    np.array([0, 1])
    ]


# check whether point is outside the map
def is_out(pos):
    global map

    if pos[1] < 0 or pos[1] >= len(map):
        return True
    if pos[0] < 0 or pos[0] >= len(map[pos[1]]):
        return True
    return False


map = []
start = []
end = []
# lets define the height we can climb, who knows ...
climb_height = 1

# Read in the file line by line in the map
# store coordinates of start and end and replace S/E by a/z
with open('day12.txt') as f:
    for line in f.readlines():
        if 'S' in line:
            start = np.array([line.index('S'), len(map)])
            line = line.replace('S', 'a')
        if 'E' in line:
            end = np.array([line.index('E'), len(map)])
            line = line.replace('E', 'z')
        map.append([ord(x) - ord('a') for x in line.strip()])

# loop over both parts as the changes are minor
for part in range(2):
    # set starting position
    if part == 0:
        last_list = [ start ]
    else:
        last_list = [ end ]
    number_of_steps = 0
    search_end = False

    # create a map for visited position
    visited = []
    # reset visited
    for line in map:
        visited.append([0 for i in range(len(line))])

    # The first solution found is by design the shortest
    # Thus, only continue until a solution is found
    while not search_end:
        # new path ends to be further investigated
        next_list = []
        # loop over all path ends (i.e. one more step)
        number_of_steps += 1
        for last in last_list:
            height_last = map[last[1]][last[0]]

            # from last position go in all 4 directions
            for dir in directions:
                next = last + dir
                # outside of map?
                if is_out(next):
                    continue
                # already visited?
                if visited[next[1]][next[0]]:
                    continue
                height_next = map[next[1]][next[0]]
                # is height difference ok?
                # check has to be reversed for part 2
                if ((part == 0 and height_next - height_last > climb_height)
                   or (part == 1 and height_last - height_next > climb_height)):
                    continue
                # save for further processing
                next_list.append(next)
                # mark position as visited
                visited[next[1]][next[0]] = 1

        # if no new pathes stop searching
        if len(next_list) == 0:
            break

        # check whether one of the positions the final one
        for next in next_list:
            # stop condition: reach end or 'a' (=0)
            if ((part == 0 and (next == end).all())
               or (part == 1 and map[next[1]][next[0]] == 0)):
                search_end = True
                break

        # repeat procedure for new last positions
        last_list = next_list

    if not search_end:
        print(f"Part {part+1}: no path found!")
    else:
        # number_of_steps is one to high
        print(f"Part {part+1}: minimum number of steps: {number_of_steps}")

