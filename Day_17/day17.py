# -*- coding: utf-8 -*-
"""
Created on Sun Dec 18 17:44:25 2022

@author: VH

For part 2: check for repeating sequences and calculate result

"""

import time

start = time.time()

class Shape:

    all = []
    counter = 0    

    def __init__(self, lines):
        self.lines = lines
        self.width = len(lines[0])
        self.height = len(lines)
        self.first = [self.height-1 for x in range(self.width)]
        self.last = [0 for x in range(self.width)]
        self.left = [self.width-1 for x in range(self.height)]
        self.right = [0 for x in range(self.height)]
        for h in range(self.height):
            for i in range(self.width):
                if lines[h][i] == '#': 
                    if self.first[i] > h:
                        self.first[i] = h
                    if self.last[i] < h:
                        self.last[i] = h
                    if self.left[h] > i:
                        self.left[h] = i
                    if self.right[h] < i:
                        self.right[h] = i
        Shape.all.append(self)

    @staticmethod
    def reset():
        Shape.counter = 0        

    @staticmethod
    def next_shape():
        new_shape = Shape.all[Shape.counter]
        Shape.counter = (Shape.counter + 1) % len(Shape.all)
        return new_shape


class Object:
    
    def __init__(self,shape,x,y):
        self.x = x
        self.y = y
        self.shape = shape
        
    def push(self, dir, max_x, lines):
        max_y = len(lines) - 1
        # check chamber size
        # at the left or right wall
        if (not (dir>0 and self.x + self.shape.width < max_x) 
            and not (dir < 0 and self.x > 0)):
            return
        for sy in range(self.shape.height):
            y = self.y + sy
            # don't worry, we are above everything
            if y > max_y:
                break
            line = lines[y]
            # we are in the area of the deposited objects
            if dir<0 and line[self.x + self.shape.left[sy] - 1] == 0:
                continue
            if dir>0 and line[self.x + self.shape.right[sy] + 1] == 0:
                continue
            return
        self.x += dir
        return
                
    def falls(self, lines):
        max_y = len(lines) - 1
        # we are high above
        if self.y - 1 > max_y:
            self.y -= 1
            return True
        # check wether there is space below
        is_space = True
        for sy in range(0, self.shape.height):
            if self.y + sy - 1 > max_y:
                break
            for sx in range(0, self.shape.width):
                if (self.shape.lines[sy][sx] == '#' and
                    lines[self.y + sy - 1][self.x + sx]) == 1:
                        is_space = False
        if is_space:
            self.y -= 1
        return is_space
            
    def merge(self, lines):
        max_y = len(lines) - 1
        for sy in range(0, self.shape.height):
            while self.y + sy > max_y:
                lines.append([0 for x in range(len(lines[0]))])
                max_y += 1
            for sx in range(0, self.shape.width):
                if self.shape.lines[sy][sx] == '#':
                    lines[self.y + sy][self.x + sx] = 1
                
class Chamber:
     
    def __init__(self, pushes, width=7):
        self.width = width
        self.lines = []
        self.lines.append([1 for x in range(width)])
        self.max_height = len(self.lines) - 1
        self.pushes = pushes
        self.push_counter = 0
        self.object_counter = 0
        self.current_object = None
        self.sequence_list = []
        self.sequence_length = 0
        self.max_sequence_length = 0
        self.last_match = []
        self.first_match = []
        Shape.reset()
        
    def object_appears(self):
        self.object_counter += 1
        self.current_object = Object(Shape.next_shape(), 2, self.max_height + 1 + 3)

    def object_is_pushed(self):
        dir = self.pushes[self.push_counter]
        #print("Push",dir)
        self.push_counter = (self.push_counter + 1) % len(self.pushes)        
        self.current_object.push(dir, self.width, self.lines)

    def object_falls(self):
        #print("Fall")
        return self.current_object.falls(self.lines)

    def merge_object(self):
        self.current_object.merge(self.lines)
        self.max_height = len(self.lines) - 1
        self.current_object = False
        
        
    def print(self):
        stop = max(len(self.lines)-20, -1)
        for i in range(len(self.lines)-1, stop, -1):
            s = ''
            for c in self.lines[i]:
                if c:
                    s += '#'
                else:
                    s += '.'
            print(s)

    def check_sequence(self, max_blocks = 1000000000000):
        check = [Shape.counter, chamber.push_counter, chamber.lines[-1], 
                 chamber.object_counter, chamber.max_height ]
        
        a = [ x for x in self.sequence_list if x[0] == check[0] and x[1] == check [1]]
        
        if a:
            a[0][2] = check[2]
            a[0][3] = check[3]
            a[0][4] = check[4]
            if self.last_match and check[3] == self.last_match[3] + 1:
                self.sequence_length += 1
                if check[0] == self.first_match[0] and check[1] == self.first_match[1]:
                    start = self.first_match[3]
                    length = check[3] - start
                    height = check[4] - self.first_match[4]
                    rest = (max_blocks - start) % length
                    new_start = [ x for x in self.sequence_list if x[3] == start + rest][0]
                    self.max_height = int((max_blocks - new_start[3]) / length * height + new_start[4])                    
                    return True
                self.sequence_length += 1
            else:
                self.sequence_length = 0
                self.first_match = check
            if self.sequence_length > self.max_sequence_length:
                self.max_sequence_length = self.sequence_length
            self.last_match = check
        else:
            self.sequence_list.append(check)
        return False

        
    def print_debug(self):
        debug = []
        for a in self.lines:
            debug.append(a[:])
        self.current_object.merge(debug)
        stop = max(len(debug)-20, -1)
        for i in range(len(debug)-1, stop, -1):
            s = ''
            for c in debug[i]:
                if c:
                    s += '#'
                else:
                    s += '.'
            print(s)
        
        
        
shape_forms = [
     ['####'],
     ['.#.', '###', '.#.'],
     ['###', '..#', '..#'],
     ['#', '#', '#', '#'],
     ['##', '##']
    ]

shapes = [Shape(x) for x in shape_forms]

# Read in the file line by line
with open('day17.txt') as f:
    # read pushes
    pushes_input = f.readline().strip()
    pushes = []
    for push in pushes_input:
        if push == '<':
            pushes.append(-1)
        elif push == '>' :
            pushes.append(1)
        else:
            print("???")

# print(len(pushes))

for part in range(2):
    chamber = Chamber(pushes)
    while(True):
        chamber.object_appears()
        while True:
            chamber.object_is_pushed()
            if not chamber.object_falls():
                break
        chamber.merge_object()
        if part == 0:
            if chamber.object_counter == 2022:
                break
        else:
            if chamber.check_sequence(1000000000000):
                break

    print(f"Part {part+1}: Height is {chamber.max_height}")    
    
end = time.time()
print(f"Running time = {end - start} s")
