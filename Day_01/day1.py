# -*- coding: utf-8 -*-
"""
Created on Sat Dec  3 16:22:24 2022

@author: VH

"""

# will contain the calories for each elf
elves = {}

elf = 1
cal = 0
with open('day1.txt') as f:
    for line in f.readlines():
        line = line.strip()
        # empty line = pack for elf stops
        if line == '':
            elves[elf] = cal
            elf += 1
            cal = 0
        # sum up calories
        else:
            cal += int(line)

# find 3 elves that carries most calories
max = [0, 0, 0]
for elf in elves:
    cal = elves[elf]
    for place in range(3):
        if (cal > max[place]):
            tmp = max[place]
            max[place] = cal
            cal = tmp

print(sum(max))
