# -*- coding: utf-8 -*-
"""
Created on Sun Dec 11 13:08:45 2022

@author: VH

1) As everyone seems to like regex, I parsed the input
   with regex
   
2) Each monkey is an object of class Monkey, that stores all
   individual parameters and does the item processing. Passing
   to other monkeys is done via a receive method
   
3) The calculation rule is parsed by pythons eval() build-in

4) I don't see the importance of the statement, that the items are
   processed in sequence of the item list. That seems not relevant as all
   items are independent and the complete list is always processed
   
5) The key to part two is to keep the remainder after dividing by the 
   least common multiple of all divisors (which is just the product as all
   are prime)

"""

import re

"""
This gives a list of entries and per entry:
 ( monkey nr, list of items, calc_rule, divisor, monkey_true, monkey_false )
"""
reg = ( r"Monkey (\d+):\s+"
       "Starting items:((?:\s*\d+,?)+)\s*"
       "Operation: ([a-z =0-9*-+/]+)\s*"
       "Test: divisible by (\d+)\s*"
       "If true: throw to monkey (\d+)\s*"
       "If false: throw to monkey (\d+)"
       )

"""
 Monkey class
 
 Setup is done after creation in order to pass the monkey objects
 for receiving items
"""
class Monkey():
    
    def __init__(self, monkey_nr):
        self.monkey_nr = monkey_nr
        self.item_list = []
        self.calculation_rule = '' 
        self.divisor = 1
        self.monkey_true = None
        self.monkey_false = None
        self.inspect_count = 0

    # setup is not in init as I want to have all monkey objects available
    def setup(self, 
              item_list, 
              calculation_rule, 
              divisor,
              monkey_true,
              monkey_false,
              divisor_product = 1):
        self.item_list = item_list
        self.calculation_rule = calculation_rule
        self.divisor = int(divisor)
        self.monkey_true = monkey_true
        self.monkey_false = monkey_false
        self.divisor_product = divisor_product

    # process item list
    def process_list(self, part = 1):
        # we just loop over the list as at the end the list will be
        # empty anyhow
        for item in self.item_list:
            self.inspect_count += 1
            # apply calculation rule
            new = int(eval(self.calculation_rule, { 'old': int(item) }))
            # devide by 3 and round to the nearest integer
            if part == 1:
                new = int(new/3)
            # check whether divisible by divisor
            remainder = new % self.divisor
            # to keep the number managable we only keep the remainder after
            # dividing by all divisors (which are all happen to be prime)
            new = new % self.divisor_product
            if remainder>0:
                self.monkey_false.receive(new)
            else:
                self.monkey_true.receive(new)
        # purge item list
        self.item_list = []
                
    # receive item
    def receive(self, item):
        self.item_list.append(item)

    # output for monkey
    def __repr__(self):
        out = f"Monkey {self.monkey_nr} had {self.inspect_count:4} inspects and holds: {self.item_list}"
        return out


# process input file
with open('day11.txt') as f:
    result = re.findall(reg, f.read())

# create monkeys
monkey_list = [ Monkey(i) for i in range(len(result))]

# calculate divisor product
divisor_product = 1
for monkey_setup in result:
    divisor_product *= int(monkey_setup[3])

rounds = [20, 10000]

for part in range(2):
    # setup monkeys
    for monkey_setup in result:
        monkey_nr = int(monkey_setup[0])
        monkey_list[monkey_nr].setup(item_list=monkey_setup[1].split(','),
                                     calculation_rule=monkey_setup[2].split('=')[1],
                                     divisor=monkey_setup[3],
                                     monkey_true = monkey_list[int(monkey_setup[4])],
                                     monkey_false = monkey_list[int(monkey_setup[5])],
                                     divisor_product = divisor_product
                                     )


    for i in range(rounds[part]):
        #print(f"\nRound {i+1}:")
        for monkey in monkey_list:
            monkey.process_list(part = part + 1)
        #for monkey in monkey_list:
            #print(monkey)

    # get inspection counts
    inspects = [monkey.inspect_count for monkey in monkey_list]
    inspects.sort()
    print(f"Part {part+1} result is ", inspects[-1]*inspects[-2])

    