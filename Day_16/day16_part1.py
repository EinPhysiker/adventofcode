# -*- coding: utf-8 -*-
"""
Created on Fri Dec 16 14:22:40 2022

@author: VH

Part 1 is finished.

Strategy: First find the distances from each valve to all others
and then search the tree by ignoring open valves and those with
flow zero. Instead go directly to the valves which are still to
be opened. If all valves are open, just sit and wait. For optimization
I use the hypothetical total pressure for sit and wait until the end.

"""

import re

reg_line = (r"Valve ([A-Z]+) has flow rate=(\d+);"
            r" tunnels? leads? to valves? ((?:[A-Z]+,? ?)+)")
reg_list = r"([A-Z]+)"


"""
Valve class

Has a name, the rate and a list of direct neighbours

Calculates minimum distance to other valves

"""


class Valve:

    # a dictionary of all valves
    all = {}

    @staticmethod
    def assign_neighbours():
        for valve in Valve.all.values():
            for name in re.findall(reg_list, valve.neighbour_string):
                valve.neighbours.append(Valve.all[name])

    def __init__(self, args):
        self.name = args[0]
        self.rate = int(args[1])
        self.neighbour_string = args[2]
        self.neighbours = []
        self.distance = {}
        self.busy = False
        Valve.all[self.name] = self

    # calculate minimum distance to other valves
    def distance_to(self, valve):
        if self == valve:
            return -1
        # if distance is already calculated
        if valve in self.distance:
            return self.distance[valve]
        # if not we need to do that
        # we assign a -1 to indicate that to avoid recursive loops
        self.busy = True
        min = -1
        for v in self.neighbours:
            if v.busy:
                continue
            # direct neighbour
            if v == valve:
                min = 1
                break
            dist = v.distance_to(valve) + 1
            if dist == 0:
                continue
            if min < 0 or dist < min:
                min = dist
        self.busy = False
        if min > 0:
            self.distance[valve] = min
        return min

    # find maximum pressure
    def find_maximum_pressure(self, status):
        current_status = Status(status)
        for valve in Valve.all.values():
            # if we are this valve or if there is no way to the valve
            if self == valve or self.distance_to(valve) <= 0:
                continue
            # if valve is open or has no flow no point to go there
            if status.flow_on[valve] or valve.rate == 0:
                continue
            # copy status for this test branch
            this_status = Status(status)
            # we accummulate pressure until valve is open
            # returns False if time runs out
            if not this_status.go_to_and_open_valve(self, valve):
                continue
            # continue recursively in branch
            valve.find_maximum_pressure(this_status)
            # if this branch is better, save it
            if this_status.max_total_pressure > current_status.max_total_pressure:
                current_status.update(this_status)
        status.update(current_status)
        return 

    def __repr__(self):
        return f"Valve {self.name}"


class Status:

    def __init__(self, other=None, time_to_go=30):
        if other:
            self.flow_on = dict(other.flow_on)
            self.total_rate = other.total_rate
            self.total_pressure = other.total_pressure
            self.max_total_pressure = other.max_total_pressure
            self.time_left = other.time_left
        else:
            self.flow_on = {}
            for valve in Valve.all.values():
                self.flow_on[valve] = False
            self.total_rate = 0
            self.total_pressure = 0
            self.max_total_pressure = 0
            self.time_left = time_to_go

    def update(self, other):
            for valve in Valve.all.values():
                self.flow_on[valve] = other.flow_on[valve]
            self.total_rate = other.total_rate
            self.total_pressure = other.total_pressure
            self.max_total_pressure = other.max_total_pressure
            self.time_left = other.time_left

    def __repr__(self):
        return (f"Total rate:         {self.total_rate}\n"
                f"Total pressure:     {self.total_pressure}\n"
                f"Max total pressure: {self.max_total_pressure}\n"
                f"Time left:          {self.time_left}")

    def go_to_and_open_valve(self, from_valve, to_valve):
        # time to go to valve and open it
        time_needed = from_valve.distance_to(to_valve) + 1
        # accummulate pressure
        self.total_pressure += self.total_rate * time_needed
        # reduce available time
        self.time_left -= time_needed
        if self.time_left < 0:
            return False
        # open valve
        self.flow_on[to_valve] = True
        self.total_rate += to_valve.rate
        # calculate possible maximum by waiting
        self.max_total_pressure = self.total_pressure + self.time_left * self.total_rate
        return True      

def find_maximum_pressure(valve1, valve2, status):
    pass
    
    
# Read in the file line by line
with open('day16.txt') as f:
    # decode sensors
    result = re.findall(reg_line, f.read())
    # create a list of Valve objects from the input
    valves = [Valve([x for x in input]) for input in result]
    # fill the neighbour list with objects
    Valve.assign_neighbours()


# print matrix of distances
# for v in Valve.all.values():
#   print([v.distance_to(v2) for v2 in Valve.all.values()])

status = Status(time_to_go=30)
Valve.all['AA'].find_maximum_pressure(status)

print(f"Part 1: most presure to be released: {status.max_total_pressure}")
