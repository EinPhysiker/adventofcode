# -*- coding: utf-8 -*-
"""
Created on Fri Dec 16 14:22:40 2022

@author: VH

With the strategy for part 1 it's difficult as me and the elephant
walk different distances to the valves, so we cannot simply check
pairs. A step by step algorithm might be better?

This tries to use the strategy of part 1 with tree branches whenever
one of the two reaches a valve and need to continue

Probably not the fastest, takes about 5 minutes.

"""

import re
import time

reg_line = (r"Valve ([A-Z]+) has flow rate=(\d+);"
            r" tunnels? leads? to valves? ((?:[A-Z]+,? ?)+)")
reg_list = r"([A-Z]+)"


"""
Valve class

Has a name, the rate and a list of direct neighbours

Calculates minimum distance to other valves

"""


class Valve:

    # a dictionary of all valves
    all = {}
    working = []

    @staticmethod
    def assign_neighbours():
        for valve in Valve.all.values():
            for name in re.findall(reg_list, valve.neighbour_string):
                valve.neighbours.append(Valve.all[name])

    def __init__(self, args):
        self.name = args[0]
        self.rate = int(args[1])
        self.neighbour_string = args[2]
        self.neighbours = []
        self.distance = {}
        self.busy = False
        Valve.all[self.name] = self
        if self.rate > 0 and self not in Valve.working:
            Valve.working.append(self)
        Valve.working.sort(key=lambda x: x.rate, reverse=True)

    # calculate minimum distance to other valves
    def distance_to(self, valve):
        if self == valve:
            return -1
        # if distance is already calculated
        if valve in self.distance:
            return self.distance[valve]
        # if not we need to do that
        # we assign a -1 to indicate that to avoid recursive loops
        self.busy = True
        min = -1
        for v in self.neighbours:
            if v.busy:
                continue
            # direct neighbour
            if v == valve:
                min = 1
                break
            dist = v.distance_to(valve) + 1
            if dist == 0:
                continue
            if min < 0 or dist < min:
                min = dist
        self.busy = False
        if min > 0:
            self.distance[valve] = min
        return min

    # find maximum flow
    def maximum_flow(self, status):
        current_status = Status(status)
        for valve in Valve.working:
            # if we are this valve or if there is no way to the valve
            if self == valve or self.distance_to(valve) <= 0:
                continue
            # if valve is open or has no flow no point to go there
            if status.flow_on[valve]:
                continue
            # copy status for this test branch
            this_status = Status(status)
            # we accummulate pressure until valve is open
            # returns False if time runs out
            if not this_status.go_to_and_open_valve(self, valve):
                continue
            # continue recursively in branch
            valve.maximum_flow(this_status)
            # if this branch is better, save it
            if this_status.max_total_pressure > current_status.max_total_pressure:
                current_status.update(this_status)
        status.update(current_status)
        return 

    def __repr__(self):
        return f"Valve {self.name}"


class Status:

    def __init__(self, other=None, time_to_go=30):
        if other:
            self.to_valve1 = other.to_valve1
            self.to_valve2 = other.to_valve2
            self.to_go1 = other.to_go1
            self.to_go2 = other.to_go2
            self.flow_on = dict(other.flow_on)
            self.total_rate = other.total_rate
            self.total_pressure = other.total_pressure
            self.max_total_pressure = other.max_total_pressure
            self.time_left = other.time_left
        else:
            self.to_valve1 = None
            self.to_valve2 = None
            self.to_go1 = 0
            self.to_go2 = 0
            self.flow_on = {}
            for valve in Valve.all.values():
                if valve.rate > 0:
                    self.flow_on[valve] = False
                else:
                    self.flow_on[valve] = True                    
            self.total_rate = 0
            self.total_pressure = 0
            self.max_total_pressure = 0
            self.time_left = time_to_go

    def update(self, other):
        self.to_valve1 = other.to_valve1
        self.to_valve2 = other.to_valve2
        self.to_go1 = other.to_go1
        self.to_go2 = other.to_go2
        for valve in Valve.all.values():
            self.flow_on[valve] = other.flow_on[valve]
        self.total_rate = other.total_rate
        self.total_pressure = other.total_pressure
        self.max_total_pressure = other.max_total_pressure
        self.time_left = other.time_left

    def go_to_valves(self):
        if self.to_go1 == 0 or self.to_go2 == 0 or self.time_left == 0:
            return
        steps = min(self.to_go1, self.to_go2, self.time_left)
        self.to_go1 -= steps
        self.to_go2 -= steps
        self.time_left -= steps
        # accummulate pressure
        self.total_pressure += self.total_rate * steps
        # calculate possible maximum by waiting
        self.max_total_pressure = self.total_pressure + self.time_left * self.total_rate
        return

    def open_both_valves(self):
        self.total_pressure += self.total_rate
        self.flow_on[self.to_valve1] = True
        self.flow_on[self.to_valve2] = True
        self.total_rate += self.to_valve1.rate
        self.total_rate += self.to_valve2.rate
        self.time_left -= 1
        self.max_total_pressure = self.total_pressure + self.time_left * self.total_rate
        return       

    def __repr__(self):
        return (f"Total rate:         {self.total_rate}\n"
                f"Total pressure:     {self.total_pressure}\n"
                f"Max total pressure: {self.max_total_pressure}\n"
                f"Time left:          {self.time_left}")

def find_maximum_pressure(status):
    global overall_maximum    
    
    status.go_to_valves()
    if status.time_left == 0:
        return
    
    # now at least one entity arrived at a valve to open
    # have both arrived?
    if status.to_go1 + status.to_go2 == 0:
        # do we need to open both valves?
        if ((not status.flow_on[status.to_valve1] and
            not status.flow_on[status.to_valve2]) and
            status.to_valve1 != status.to_valve2):
            status.open_both_valves()
    # one has to open a valve and the other is on the way
    elif status.to_go1 == 0 and not status.flow_on[status.to_valve1]:
        status.total_pressure += status.total_rate
        status.flow_on[status.to_valve1] = True
        status.to_go2 -= 1
        status.total_rate += status.to_valve1.rate
        status.time_left -= 1
        status.max_total_pressure = status.total_pressure + status.time_left * status.total_rate
    elif status.to_go2 == 0 and not status.flow_on[status.to_valve2]:
        status.total_pressure += status.total_rate
        status.flow_on[status.to_valve2] = True
        status.to_go1 -= 1
        status.total_rate += status.to_valve2.rate
        status.time_left -= 1
        status.max_total_pressure = status.total_pressure + status.time_left * status.total_rate
        
    if status.time_left == 0:
        return        

    # no point in continuing when waiting with all valves open would not
    # increase the maximum
    if overall_maximum >= status.total_pressure + status.time_left * maximum_rate:
        return
        
    # the case where one has to open a valve and the other to go on is delayed
    # we now cover the cases to go on
    current_status = Status(status)
    
    valves_list = [x for x in Valve.working if not status.flow_on[x]]
    
    valves_left1 =[None]
    if status.to_go1 == 0 and status.flow_on[status.to_valve1]:
        # These are the valves worth to go
        valves_left1 = valves_list
    # if there are no valves to open, no point to continue walking
    if not valves_left1:
        status.total_pressure += status.total_rate * status.time_left
        status.time_left = 0
        return
    for valve1 in valves_left1:
        valves_left2 =[None]
        if status.to_go2 == 0 and (status.flow_on[status.to_valve2] or status.to_valve1 == status.to_valve2):
            # These are the valves worth to go
            valves_left2 = [x for x in valves_list if x != valve1]
        for valve2 in valves_left2:
            this_status = Status(status)
            if valve1 is not None: 
                this_status.to_go1 = this_status.to_valve1.distance_to(valve1)
                this_status.to_valve1 = valve1 
            if valve2 is not None:
                this_status.to_go2 = this_status.to_valve2.distance_to(valve2)
                this_status.to_valve2 = valve2
            
            find_maximum_pressure(this_status)
            
            if this_status.max_total_pressure > current_status.max_total_pressure:
                if this_status.max_total_pressure > overall_maximum:
                    overall_maximum = this_status.max_total_pressure
                current_status.update(this_status)                
    
    status.update(current_status)
    return 

start = time.time()
    
# Read in the file line by line
with open('day16.txt') as f:
    # decode sensors
    result = re.findall(reg_line, f.read())
    # create a list of Valve objects from the input
    valves = [Valve([x for x in input]) for input in result]
    # fill the neighbour list with objects
    Valve.assign_neighbours()


# print matrix of distances
# for v in Valve.all.values():
#   print([v.distance_to(v2) for v2 in Valve.all.values()])

overall_maximum = 0
maximum_rate = sum([x.rate for x in Valve.working])
status = Status(time_to_go=26)
status.to_valve1 = Valve.all['AA']
status.to_valve2 = Valve.all['AA']
find_maximum_pressure(status)

print(f"Part2: most presure to be released: {status.max_total_pressure}")

end = time.time()
print(f"Running time = {end-start} s")