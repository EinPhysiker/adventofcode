#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Dec 13 18:14:41 2022

@author: VH

Nested Python lists as input? Seriously?

And if part1 is done properly, part2 is for free.

"""

import re
import functools

reg = r"([\[\]\d,]+)"


# compare function
def compare(item1, item2):
    # genuine integer comparison
    if type(item1) == int and type(item2) == int:
        # this is not nice, I know, but as python 3 has no cmp ...
        # it returns -1 when smaller, 0 when equal and 1 when larger
        return (item1 > item2) - (item1 < item2)

    # so, at least one is a list, and the other needs to be, too
    if not type(item1) == list:
        item1 = [item1]
    if not type(item2) == list:
        item2 = [item2]

    # now compare pairwise, the number of pairs is determined by
    # the length of the shorter list
    for a, b in zip(item1, item2):
        result = compare(a, b)
        # if we have a decision, return
        if result != 0:
            return result

    # if no decision yet, all elements with partners are equal,
    # so size matters
    l1 = len(item1)
    l2 = len(item2)
    return (l1 > l2) - (l1 < l2)


with open('day13.txt') as f:
    result = [eval(x) for x in re.findall(reg, f.read())]
    pairs = list(zip(*[iter(result)]*2))

sum = 0
index = 0
for a, b in pairs:
    index += 1
    if compare(a, b) < 1:
        sum += index

print(f"Part1: Sum of indices of correct pairs = {sum}")

result.append([[2]])
result.append([[6]])

ordered = sorted(result, key=functools.cmp_to_key(compare))

pos1 = ordered.index([[2]]) + 1
pos2 = ordered.index([[6]]) + 1
print(f"Part2: Decoder key for distress signal is {pos1*pos2}")
