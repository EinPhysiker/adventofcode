# Advent of Code 2022

All tasks are available at https://adventofcode.com/2022

All tasks are solved in Python. Maybe sometimes other languages are used in addition. 

I also tried to keep the use of very specialized packages at a minimum.

I know, it's not the nicest code most of the time, but it works!

## Files

The tasks for the various days are now put into individual directories. The files are still named dayX.* with *=txt for the input files and the others for the code.

## Environment

Anaconda with python 3.10 on Windows 10 / Linux using Spyder

Additional packages:


| package | usage |
| ------ | ------ |
| matplotlib | day 8,9,12,14 for visualization (not part of the task) |
| numpy      | day 9,12,14 for vector operations                      |
| re         | day 11,13,14 for parsing input file                    |
| functools  | day 13 for sorting lists with a compare function    |
