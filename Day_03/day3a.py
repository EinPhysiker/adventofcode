# -*- coding: utf-8 -*-
"""
Created on Sat Dec  3 17:19:06 2022

@author: VH
"""

total = 0
with open('day3.txt') as f:
    for line in f.readlines():
        line = line.strip()
        length2 = int(len(line)/2)
        part1 = line[0:length2]
        part2 = line[length2:]
        found = ''
        for a in part1:
            for b in part2:
                if a == b:
                    found = a
        val = ord(found) - ord('a') + 1
        if (val < 1 or val > 26):
            val = ord(found) - ord('A') + 27
        total += val

print(total)
