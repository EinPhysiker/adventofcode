# -*- coding: utf-8 -*-
"""
Created on Sat Dec  3 17:19:06 2022

@author: VH
"""

total = 0
with open('day3.txt') as f:
    while True:
        group = []
        for n in range(3):
            line = f.readline()
            group.append(line.strip())

        print(group)

        found = ''
        for a in group[0]:
            for b in group[1]:
                if a == b:
                    for c in group[2]:
                        if a == c:
                            found = a

        if found == '':
            break
        val = ord(found) - ord('a') + 1
        if (val < 1 or val > 26):
            val = ord(found) - ord('A') + 27
        total += val

print(total)
