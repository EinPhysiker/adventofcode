# -*- coding: utf-8 -*-
"""
Created on Sat Dec  3 16:42:18 2022

@author: VH
"""

result = {}

for opp in ['A', 'B', 'C']:
    for res in ['X', 'Y', 'Z']:
        type = opp + ' ' + res
        ans = ord(opp) + ord(res) - ord('Y')
        if ans < ord('A'):
            ans += 3
        if ans > ord('C'):
            ans -= 3
        me = chr(ans)
        print(type, me)

        val = ord(opp) - ord(me)
        score = 0
        if val in [0]:
            score = 3
        elif val in [-1, 2]:
            score = 6
        score += ord(me) - ord('A') + 1
        result[type] = score

total = 0
with open('day2.txt') as f:
    for line in f.readlines():
        line = line.strip()
        total += result[line]

print(total)
