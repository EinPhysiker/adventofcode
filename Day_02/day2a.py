# -*- coding: utf-8 -*-
"""
Created on Sat Dec  3 16:42:18 2022

@author: VH
"""

base = ord('X') - ord('A')
result = {}

for opp in ['A', 'B', 'C']:
    for me in ['X', 'Y', 'Z']:
        type = opp + ' ' + me
        val = ord(opp) - ord(me) + base
        score = 0
        if val in [0]:
            score = 3
        elif val in [-1, 2]:
            score = 6
        score += ord(me) - ord('X') + 1
        result[type] = score

total = 0
with open('day2.txt') as f:
    for line in f.readlines():
        line = line.strip()
        total += result[line]

print(total)
