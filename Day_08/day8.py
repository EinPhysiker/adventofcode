# -*- coding: utf-8 -*-
"""
Created on Thu Dec  8 14:17:56 2022

@author: VH

example input

30373
25512
65332
33549
35390

"""

import matplotlib.pyplot as plt

with open('day8.txt') as f:
    # strip lines of any characters like crlf etc.
    lines = [line.strip() for line in f.readlines()]

    matrix = []
    for line in lines:
        row = []
        for char in line:
            row.append(int(char))
        matrix.append(row)

visibility_matrix = []
# check rows for visibility from left
for row_nr in range(0, len(matrix)):
    visibility_row = []
    max_height = -1
    for column_nr in range(0, len(matrix[row_nr])):
        height = matrix[row_nr][column_nr]
        if height > max_height:
            max_height = height
            visibility_row.append(True)
        else:
            visibility_row.append(False)
    visibility_matrix.append(visibility_row)

# check rows for visibility from right
# True has priority over False
for row_nr in range(0, len(matrix)):
    max_height = -1
    for column_nr in range(len(matrix[row_nr])-1, -1, -1):
        height = matrix[row_nr][column_nr]
        if height > max_height:
            max_height = height
            visibility_matrix[row_nr][column_nr] = True

# check columns for visibility from top
# True has priority over False
for column_nr in range(0, len(matrix[0])):
    max_height = -1
    for row_nr in range(0, len(matrix)):
        height = matrix[row_nr][column_nr]
        if height > max_height:
            max_height = height
            visibility_matrix[row_nr][column_nr] = True

# check columns for visibility from bottom
# True has priority over False
for column_nr in range(0, len(matrix[0])):
    max_height = -1
    for row_nr in range(len(matrix)-1, -1, -1):
        height = matrix[row_nr][column_nr]
        if height > max_height:
            max_height = height
            visibility_matrix[row_nr][column_nr] = True

# now count the number of visible trees
count = 0
for row in visibility_matrix:
    for tree_visibility in row:
        if tree_visibility:
            count += 1

print(f"{count} trees are visible")

# ok, now calculate the scenic view of each tree, which is the product
# of the numbers of trees to up, left, down, right, until and including
# a tree of same or larger height

# we skip all edge trees as one factor is 0 anyway

scenic_matrix = []
for row_nr in range(1, len(matrix)-1):
    scenic_row = []
    for column_nr in range(1, len(matrix[row_nr])-1):
        height = matrix[row_nr][column_nr]
        left = 0
        for col in range(column_nr-1, -1, -1):
            left += 1
            if matrix[row_nr][col] >= height:
                break
        right = 0
        for col in range(column_nr+1, len(matrix[row_nr])):
            right += 1
            if matrix[row_nr][col] >= height:
                break
        up = 0
        for row in range(row_nr-1, -1, -1):
            up += 1
            if matrix[row][column_nr] >= height:
                break
        down = 0
        for row in range(row_nr+1, len(matrix)):
            down += 1
            if matrix[row][column_nr] >= height:
                break
        scenic_row.append(up * down * left * right)
    scenic_matrix.append(scenic_row)


# find the largest scenic value
# add-on: find the position for plotting
max_scenic_value = 0
max_row = -1
max_column = -1
row_nr = 0
for row in scenic_matrix:
    row_nr += 1
    max_in_row = max(row)
    column_nr = row.index(max_in_row)
    if max_in_row > max_scenic_value:
        max_scenic_value = max_in_row
        max_row = row_nr
        max_column = column_nr

print(f"Maximum scenic value is {max_scenic_value}")

plt.figure(figsize=(9, 9))

# plot matrix
plt.imshow(matrix, cmap='Greens', interpolation='Nearest')

# plot visibility on top
plot_point = False
for row_nr in range(0, len(matrix)):
    for column_nr in range(0, len(matrix[row_nr])):
        if visibility_matrix[row_nr][column_nr]:
            plt.scatter(column_nr, row_nr, s=4, marker='.', color="red")

# plot point with max scenic view
plt.scatter(max_column, max_row, s=64, marker='+', color="blue")
plt.show()
