# -*- coding: utf-8 -*-
"""
Created on Tue Dec  6 11:40:59 2022

@author: VH
"""


# a function for a change:
# search first occurance of n consequtive different characters in a message
# the task is to find the position of the last(!) character
def find_unique_characters(message, length):
    for pos in range(length, len(message)):
        # process line in blocks of "length" characters
        marker = line[pos-length:pos]
        # if the set length is equal to the string length, all characters
        # are different
        if len(set(marker)) == length:
            return pos


# first open the file and read it line by line
# (probably only 1 line present, but anyway)
with open('day6.txt') as f:
    for line in f.readlines():
        # check 4 characters
        packet_marker = find_unique_characters(line.strip(), length=4)
        print("End of packet marker at position", packet_marker)
        message_marker = find_unique_characters(line.strip(), length=14)
        print("End of message marker at position", message_marker)
