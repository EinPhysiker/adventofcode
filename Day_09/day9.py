# -*- coding: utf-8 -*-
"""
Created on Fri Dec  9 10:08:19 2022

@author: VH

Example inputs from the webpage are
day9_test_1.txt for part1
day9_test_2.txt for part2

"""

import numpy as np
import matplotlib.pyplot as plt

# dictionary: directions into coordinate changes
directions = {
       'R': np.array([1, 0]),
       'D': np.array([0, -1]),
       'L': np.array([-1, 0]),
       'U': np.array([0, 1])
       }

# for visualization
fig, ax = plt.subplots(2, figsize=(4, 7))
fig.suptitle("Advent of Code: Day 9")

# two different rope length
rope_length = [2, 10]

for part in range(2):
    length = rope_length[part]

    # start positions
    pos = []
    for n in range(length):
        pos.append(np.array([0, 0]))

    # set of visited positions
    visited = [pos[length-1].tolist()]

    with open('day9.txt') as f:
        # strip lines of any characters like crlf etc.
        moves = [line.strip().split() for line in f.readlines()]

        for move in moves:
            dir = directions[move[0]]
            count = int(move[1])

            for n in range(count):
                pos[0] += dir

                for n in range(1, length):
                    diffpos = pos[n-1] - pos[n]
                    # 1.5 instead of sqrt(2)
                    if np.linalg.norm(diffpos) > 1.5:
                        # action on rope knot is needed
                        # we need to add up offsets on row and column
                        # as for ropes longer than 2 a |diff| of [2,2]
                        # is possible
                        offset = np.array([0, 0])
                        if diffpos[0] == 2:
                            offset += directions['L']
                        elif diffpos[0] == -2:
                            offset += directions['R']
                        if diffpos[1] == 2:
                            offset += directions['D']
                        elif diffpos[1] == -2:
                            offset += directions['U']
                        pos[n] = pos[n-1] + offset
                if pos[length-1].tolist() not in visited:
                    visited.append(pos[length-1].tolist())

    print(f"Length = {length}: number of unique visited positions by tail: {len(visited)}")

    ax[part].plot([pos[0] for pos in visited],
                  [pos[1] for pos in visited],
                  color='blue')

plt.tight_layout()
plt.show()
