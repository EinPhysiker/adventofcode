# -*- coding: utf-8 -*-
"""
Created on Sat Dec 10 10:44:15 2022

@author: VH

This is now a solution with two modules called once per clock.
cpu has always to be called last as the registers reflect the 
state after the cycle, not during.

"""

class cpu():
        
    def __init__(self, input = 'day10.txt', regX = 1):
        self.clock = 0
        self.cmd_clock = 0
        self.cmd = None
        self.args = None
        self.input = open(input)
        self.regX = regX
        self.debug_sum = 0
        
        # read first command
        self.read_next()

    def run_once(self):
        self.clock += 1
        if self.clock in range(20,221,40):
            self.debug_sum += self.clock * self.regX
        self.cmd_clock += 1
        if self.cmd_clock >= self.cmds[self.cmd]['clk']:
            self.cmds[self.cmd]['func'](self, self.args)
            return self.read_next()
        return True            

    def read_next(self):
        next_cmd = self.input.readline()
        if not next_cmd:
            return False
        next_cmd = next_cmd.strip().split()
        self.cmd = next_cmd[0]
        self.args = next_cmd[1:]
        self.cmd_clock = 0
        return True        

    def get_regX(self):
        return self.regX

    def get_debug_sum(self):
        return self.debug_sum
            
    # commands
    def noop(self, args):
        pass
    
    def addx(self, args):
        self.regX += int(args[0])
        
    cmds = { 
        'noop': {'func': noop, 
                 'clk': 1}, 
        'addx': {'func': addx,
                 'clk': 2} 
        }

class crt():
    
    def __init__(self):
        self.display = []
        self.current_line = ''
        
    def run_once(self, regX):
        if len(self.current_line) in [ regX - 1, regX, regX + 1]:
            self.current_line += '#'
        else:
            self.current_line += '.'
        
        if len(self.current_line) == 40:
            self.display.append(self.current_line)
            self.current_line = ''
            
    def __repr__(self):
        out = 'CRT Display\n'
        for line in self.display:
            out += f"{line}\n"
        return out

my_cpu = cpu()
my_crt = crt()

while True: 
    my_crt.run_once(my_cpu.get_regX())
    if not my_cpu.run_once():
        break
    
print(my_cpu.get_debug_sum())

print(my_crt)    
    
    