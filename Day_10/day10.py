# -*- coding: utf-8 -*-
"""
Created on Sat Dec 10 10:44:15 2022

@author: VH
"""

with open('day10_test.txt') as f:
    # strip lines of any characters like crlf etc.
    cmds = [line.strip().split() for line in f.readlines()]
    
    value_x = 1
    history = [ value_x ]
    
    for cmd in cmds:
        if cmd[0] == "noop":
            history.append(value_x)
        elif cmd[0] == "addx":
            # value_x only change after finishing 2 cycles
            history.append(value_x)
            history.append(value_x)
            offset = int(cmd[1])
            value_x += offset
    # store last value
    history.append(value_x)

# part 1
# signal strength = cycle * value_x
# result = sum of signal strength
sum_signal_strength = 0
for cycle in range(20,221,40):
    sum_signal_strength += cycle * history[cycle]
print(f"Total signal strength = {sum_signal_strength}")    
    
# part 2
# normally one would sync parsing and drawing for real time display, but 
# as I have the results as array already I'm too lazy to include this in
# the parsing loop

print("\nCRT Display:\n")
for row in range(6):
    crt_line = ''
    for dot in range(40):
        cycle = 40 * row + dot + 1
        value = history[cycle]
        if dot in [ value-1, value, value+1 ]:
            crt_line += '#'
        else:
            crt_line += '.'
    print(f"{crt_line}")
    
